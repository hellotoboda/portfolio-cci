<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TagFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        
        for($i = 0; $i<15; $i++)
        {
            $tag = new Tag();
            $tag->setLabel("Tag $i");
            $this->addReference("tag_$i", $tag);
            $manager->persist($tag);
        }

        $manager->flush();
    }
}
