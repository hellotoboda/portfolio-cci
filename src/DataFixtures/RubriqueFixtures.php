<?php

namespace App\DataFixtures;

use App\DataFixtures\Trait\FixturesTrait;
use App\Entity\Rubrique;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RubriqueFixtures extends Fixture implements DependentFixtureInterface
{
    use FixturesTrait;

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $count = 0;
        for ($i=0; $i < $this::NB_TAG ; $i++) { 
            for ($j=0; $j < $this::NB_RUBRIQUE ; $j++) { 
                $rub = new Rubrique();
                $rub->setLabel("Rubrique $i $j");
                $rub->setTag($this->getReference("tag_$i"));
                $this->addReference("rub_$count", $rub);
                $manager->persist($rub);
                $count++;
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            TagFixtures::class
        ];
    }
}
