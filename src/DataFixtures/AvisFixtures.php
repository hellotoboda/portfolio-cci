<?php

namespace App\DataFixtures;

use App\Entity\Avis;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AvisFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        for($i = 0; $i<10; $i++){
            $avis = new Avis();
            $avis->setLabel("Fort interessant");
            $avis->setDateCreation(new DateTime("now"));
            $avis->setProjet($this->getReference("projet1"));
            $manager->persist($avis);

            $avis2 = new Avis();
            $avis2->setLabel("Fort interessant $i");
            $avis2->setDateCreation(new DateTime("now"));
            $avis2->setProjet($this->getReference("projet2"));
            $manager->persist($avis2);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ProjetFixtures::class,
        ];
    }
}
