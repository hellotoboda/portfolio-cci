<?php

namespace App\DataFixtures;

use App\Entity\Contact;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ContactFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for($i=0; $i<7; $i++){
            $contact = new Contact();
            $contact->setMail("test$i@test.fr");
            $contact->setObjet("test des fixtures");
            $contact->setCorps("Corps de la fixtures $i");
            $manager->persist($contact);
        }
        $manager->flush();
    }
}
