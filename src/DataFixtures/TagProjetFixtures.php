<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TagProjetFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        
        for($i = 0; $i<15; $i++)
        {
            $tag = $this->getReference("tag_$i");
            if($i%2 == 0){
                $tag->addProjet($this->getReference("projet1"));
            }
            else{
                $tag->addProjet($this->getReference("projet2"));
            }
            $manager->persist($tag);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ProjetFixtures::class, 
            TagFixtures::class
        ];
    }
}
