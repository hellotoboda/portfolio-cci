<?php

namespace App\DataFixtures;

use App\Entity\Temoignage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TemoignageFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        for ($i=0; $i <5 ; $i++) {
            $tem = new Temoignage();
            $tem->setCommentaire("Temoignage de $i");
            $tem->setNom($this->getReference("info")->getNom());
            $tem->setPrenom($this->getReference("info")->getPrenom());
            $manager->persist($tem);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UtilisateurFixtures::class
        ];
    }
}
