<?php

namespace App\DataFixtures;

use App\Entity\Projet;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProjetFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $projet = new Projet();
        $projet->setTitre("Projet portfolio symfony");
        $projet->setDescription("Projet qui décrit le portfolio de l'individu");
        $projet->setDateProjet(new DateTime("now"));
        $this->addReference("projet1", $projet);
        $manager->persist($projet);

        $projet2 = new Projet();
        $projet2->setTitre("Projet bacasable symfony");
        $projet2->setDescription("Projet pédagogique de symfony");
        $projet2->setDateProjet(new DateTime("now"));
        $this->addReference("projet2", $projet2);
        $manager->persist($projet2);

        $manager->flush();
    }
}
