<?php

namespace App\DataFixtures;

use App\DataFixtures\Trait\FixturesTrait;
use App\Entity\Reseau;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ReseauFixtures extends Fixture implements DependentFixtureInterface
{
    use FixturesTrait;
    public function load(ObjectManager $manager): void
    {
        $admin = $this->getReference("admin");
        $utilisateur = $this->getReference("user");

        $tabFix = [$admin, $utilisateur];
        foreach ($this::res_soc as $key => $value) {
            foreach ($tabFix as $u) {
                $res = new Reseau();
                $res->setReseau($key);
                $res->setValeur($value);
                $res->setUtilisateur($u);
                $manager->persist($res);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UtilisateurFixtures::class
        ];
    }
}
