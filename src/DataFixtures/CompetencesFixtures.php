<?php

namespace App\DataFixtures;

use App\DataFixtures\Trait\FixturesTrait;
use App\Entity\Competence;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CompetencesFixtures extends Fixture implements DependentFixtureInterface
{
    use FixturesTrait;
    
    public function load(ObjectManager $manager): void
    {
        $count = 0;
        for ($i=0; $i < $this::NB_TAG*$this::NB_RUBRIQUE ; $i++) { 
            for ($j=0; $j < $this::NB_COMP ; $j++) { 
                $comp = new Competence();
                $comp->setLabel("Rubrique $i $j");
                $comp->setRubrique($this->getReference("rub_$i"));
                $this->addReference("comp_$count", $comp);
                $manager->persist($comp);
                $count++;
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RubriqueFixtures::class
        ];
    }
}
