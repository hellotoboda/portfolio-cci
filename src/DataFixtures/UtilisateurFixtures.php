<?php

namespace App\DataFixtures;

use App\Entity\Infos;
use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UtilisateurFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager)
    {
        $user = new Utilisateur();
        $infosUser = new Infos();
        $user->setEmail('user@user.fr');
        $password = $this->hasher->hashPassword($user, 'coucou');
        $user->setPassword($password);
        $infosUser->setNom("user");
        $infosUser->setPrenom("test");
        $infosUser->setUtilisateur($user);
        $infosUser->setDescription("Je suis une description");
        $manager->persist($user);
        $manager->persist($infosUser);
        $this->addReference("user", $user);
        $this->addReference("info", $infosUser);
        //$manager->flush();
        
        $admin = new Utilisateur();
        $admin->setEmail('admin@admin.fr');
        $infosAdmin = new Infos();
        $infosAdmin->setNom("admin");
        $infosAdmin->setPrenom("test");
        $infosAdmin->setDescription("Je suis une description");
        $infosAdmin->setUtilisateur($admin);
        $infosAdmin->setProprietaire(true);
        $password = $this->hasher->hashPassword($admin, 'coucou');
        $admin->setPassword($password);
        $admin->addRole("ROLE_ADMIN");
        
        $manager->persist($admin);
        $manager->persist($infosAdmin);
        $this->addReference("admin", $admin);
        $manager->flush();
        
    }
}
