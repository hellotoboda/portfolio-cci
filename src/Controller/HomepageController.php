<?php

namespace App\Controller;

use App\Repository\InfosRepository;
use App\Repository\TemoignageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HomepageController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function __invoke(TemoignageRepository $temRepo, InfosRepository $infosRepo): Response
    {
        $tems = $temRepo->findAll();
        // Recuperer les infos de l'utilisateur du portfolio => rajout de la notion proprietaire
        // j'utilise les fixtures pour gérer la notion de proprietaire
        $info = $infosRepo->findOneBy(["proprietaire" => true]);
        
        
        return $this->render('homepage.twig', [
            'controller_name' => 'HomepageController',
            "tems"=>$tems,
            "info"=>$info,
        ]);
    }
}
