<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Attribute\Route;

class ContactezNousController extends AbstractController
{
    #[Route('/contactez-nous', name: 'app_contactez_nous')]
    public function __invoke(Request $request, 
        MailerInterface $mailer, 
        ContactRepository $contactRepository,
        EntityManagerInterface $entityManager): Response
    {
        try{
            $form = $this->createForm(ContactType::class);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $email=$data["email"];
                $obj=$data["objet"];
                $corps=$data["corps"];
                $contact = new Contact();
                $contact->setMail($email);
                $contact->setCorps($corps);
                $contact->setObjet($obj);
                //$email = $this->creer_mail($email, $obj, $corps);  
                //$mailer->send($email);
                // on initialise l'action (ici l ajout)
                $entityManager->persist($contact);
                $this->addFlash("success", "initilisation de l'ajout");
                // on valide l action (ici l'ajoout)
                $entityManager->flush();
                $this->addFlash("success", "ajout effectif");
                //$contact = $contact;

                // initialiser l'action (ici de suppression)
                //$entityManager->remove($contact);
                //$entityManager->flush();
                //$this->addFlash("success", "suppression");
                
                //dd($contact);

                //dd($email);
            }
            /*
            $this->addFlash("success", "tartenpion");
            $contacts = $contactRepository->findAll();
            //$contact = $contactRepository->find(1);
            */
        }
        catch(Exception $exception){
            $this->addFlash("error", "mail non envoyé");
        }
        
        //return $this->redirectToRoute("app_homepage");

        return $this->render('anonyme/contactez_nous.html.twig', [
            'controller_name' => 'ContactezNousController',
            "form"=>$form,
        ]);
    }

    public function creer_mail($email, $obj, $corps){
        $email = (new Email())
            ->from($email)
            ->to("cornado.thibaut@gmail.com")
            ->subject($obj)
            ->text($corps)
            ;
        return $email;
    }
}
