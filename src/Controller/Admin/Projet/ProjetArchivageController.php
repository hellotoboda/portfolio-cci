<?php

namespace App\Controller\Admin\Projet;

use App\Entity\Projet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ProjetArchivageController extends AbstractController
{
    #[Route('/admin/projet/{projet}/archivage', name: 'app_projet_archivage')]
    public function index(Projet $projet, EntityManagerInterface $entityManager): Response
    {
        $projet->archivage();
        $entityManager->persist($projet);
        $entityManager->flush();
        return$this->redirectToRoute("app_projet_admin");
    }
}
