<?php

namespace App\Controller\Admin\Comp;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class AdminTagCompController extends AbstractController
{
    #[Route('/admin/competence/', name: 'app_admin_tag_comp')]
    public function index(): Response
    {
        return $this->render('admin/competence/tag/liste.twig', [
            'controller_name' => 'AdminTagCompController',
        ]);
    }
}
