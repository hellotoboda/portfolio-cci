<?php

namespace App\Controller\Admin\Contact;

use App\Repository\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ContactsController extends AbstractController
{
    #[Route('/admin/contact', name: 'app_admin_contact')]
    public function index(ContactRepository $contactRepo): Response
    {
        return $this->render('admin/contact/contacts.twig', [
            "contacts" => $contactRepo->findBy(["isReponse" => false])
        ]);
    }
}
