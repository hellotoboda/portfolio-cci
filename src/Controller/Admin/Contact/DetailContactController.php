<?php

namespace App\Controller\Admin\Contact;

use App\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DetailContactController extends AbstractController
{
    #[Route('/admin/contact/{tarte}/detail/', name: 'app_detail_contact')]
    public function index(Contact $tarte): Response
    {
        return $this->render('admin/contact/detail.twig', [
            'controller_name' => 'DetailContactController',
            "contact" => $tarte
        ]);
    }
}
