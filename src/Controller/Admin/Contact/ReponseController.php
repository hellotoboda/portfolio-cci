<?php

namespace App\Controller\Admin\Contact;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Mime\Email;

class ReponseController extends AbstractController
{
    #[Route('/admin/contact/{contact}/reponse/', name: 'app_reponse')]
    public function index(Request $request, 
    MailerInterface $mailer, 
    ContactRepository $contactRepository,
    EntityManagerInterface $entityManager,
    Contact $contact): Response
    {
        try{
            $form = $this->createForm(ContactType::class, [
                "email" => $contact->getMail(),
                "objet" =>  $contact->getObjet(),
            ]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $corps=$data["corps"];
                $contact->setReponse($corps);
                //$email = $this->creer_mail($email, $obj, $corps);  
                //$mailer->send($email);
                // on initialise l'action (ici l ajout)
                $entityManager->persist($contact);
                // on valide l action (ici l'ajoout)
                $entityManager->flush();
                $this->addFlash("success", "reponse effectuée");
            }
        }
        catch(Exception $exception){
            $this->addFlash("error", "mail non envoyé");
        }
        
        //return $this->redirectToRoute("app_homepage");

        return $this->render('admin/contact/reponse.twig', [
            "form"=>$form,
        ]);
    }

    public function creer_mail($email, $obj, $corps){
        $email = (new Email())
            ->from($email)
            ->to("cornado.thibaut@gmail.com")
            ->subject($obj)
            ->text($corps)
            ;
        return $email;
    }
}
