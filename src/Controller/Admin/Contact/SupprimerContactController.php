<?php

namespace App\Controller\Admin\Contact;

use App\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class SupprimerContactController extends AbstractController
{
    #[Route('/admin/supprimer/{tarte}/contact', name: 'app_supprimer_contact')]
    public function index(Contact $tarte, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($tarte);
        $entityManager->flush();
        return $this->redirectToRoute("app_contactez_nous");
    }
}
