<?php

namespace App\Controller\Admin\Contact;

use App\Entity\Contact;
use App\Form\ContactEntityType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ContactModifierController extends AbstractController
{
    #[Route('/admin/contact/{poulpe}/modifier', name: 'app_contact_modifier')]
    public function index(Request $request, Contact $poulpe, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ContactEntityType::class, $poulpe);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
        }
        return $this->render('contact/modifier.twig', [
            'controller_name' => 'ContactModifierController',
            "form"=> $form
        ]);
    }
}
