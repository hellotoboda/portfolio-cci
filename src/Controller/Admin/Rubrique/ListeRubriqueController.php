<?php

namespace App\Controller\Admin\Rubrique;

use App\Repository\RubriqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ListeRubriqueController extends AbstractController
{
    #[Route('/liste/rubrique', name: 'app_liste_rubrique')]
    public function index(RubriqueRepository $rubriqueRepo): Response
    {
        return $this->render('admin/rubrique/liste.twig', [
            "items" => $rubriqueRepo->findAll() 
        ]);
    }
}
