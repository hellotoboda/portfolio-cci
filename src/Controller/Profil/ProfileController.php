<?php

namespace App\Controller\Profil;

use App\Entity\Infos;
use App\Form\MDPType;
use App\Form\ProfileType;
use App\Repository\InfosRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProfileController extends AbstractController
{
    private $hasher;
    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    #[Route('/tartenpion', name: 'app_profile')]
    #[IsGranted("ROLE_UTILISATEUR")]
    public function index(Request $request,Request $requestPass, Request $requestP,SluggerInterface $slugger,InfosRepository $infosRepo, EntityManagerInterface $entityManager): Response
    {
        $utilisateur = $this->getUser();
        $infos = $infosRepo->findOneBy(["utilisateur" => $utilisateur->getId()]);
        $form = $this->createForm(ProfileType::class, [
            "email" => $utilisateur->getEmail(),
            "nom" =>  $infos->getNom(),
            "prenom" =>  $infos->getPrenom(),
            "description" =>  $infos->getDescription(),
        ]);
        $utilisateur = $this->getUser();
        $form->handleRequest($request);

        $formPass = $this->createForm(MDPType::class);
        $formPass->handleRequest($requestPass);
        if ($formPass->isSubmitted() && $formPass->isValid()) {
            //dd($formPass->getData());
            $datas = $formPass->getData();
            //dd($datas);
            $password = $this->hasher->hashPassword($utilisateur, $datas["plainpassword"]);
            $utilisateur->setPassword($password);
            $entityManager->persist($utilisateur);
            $entityManager->flush();
        }

        if ($form->isSubmitted() && $form->isValid()) {
            
            $datas = $form->getData();
            
            $uploadFile = $datas["upload"];
            if ($uploadFile) {
                $originalFilename = pathinfo($uploadFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$uploadFile->guessExtension();
                try {
                    $uploadFile->move("media/profile", $newFilename);
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $infos->setAvatar($newFilename);
            }
            $infos->setNom($form["nom"]->getData());
            $utilisateur->setEmail($form["email"]->getData());
            $infos->setPrenom($form["prenom"]->getData());
            $infos->setDescription($form["description"]->getData());
            $entityManager->persist($infos);
            $entityManager->persist($utilisateur);
            $entityManager->flush();
        }
        
        return $this->render('utilisateur/profile.twig', [
            "form"=>$form,
            "formPass" => $formPass
        ]);
    }
}