<?php

namespace App\Controller\Profil;

use App\Form\MDPType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;

class ProfilePasswordController extends AbstractController
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    #[Route('/profile/password', name: 'app_profile_password')]
    public function index(Request $request,  EntityManagerInterface $entityManager): Response
    {
        $utilisateur = $this->getUser();
        $form = $this->createForm(MDPType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $datas = $form->getData();
            $password = $this->hasher->hashPassword($utilisateur, $datas["plainpassword"]);
            $utilisateur->setPassword($password);
            $entityManager->persist($utilisateur);
            $entityManager->flush();
        }
        return $this->render('profile_password/index.html.twig', [
            'controller_name' => 'ProfilePasswordController',
            "formPass" => $form
        ]);
    }
}
