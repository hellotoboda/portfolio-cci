<?php

namespace App\Form;

use App\Entity\Projet;
use App\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre')
            ->add('description')
            ->add('dateProjet', null, [
                'widget' => 'single_text',
            ])
            ->add("tags", EntityType::class, [
                'class' => Tag::class,
                'multiple' => true,
                'choice_label' => function (Tag $tag): string {
                    return $tag->getLabel();
                },
                "mapped" => false,
                "required"=>false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Projet::class,
        ]);
    }
}
